# gmenu
**gmenu** is a dmenu-based browser for the [Gemini protocol](https://gemini.circumlunar.space).\
Proof-of-concept to be rebuilt correctly at a later date.

## Dependencies
- [dmenu](https://tools.suckless.org/dmenu)
- [gcat](https://github.com/aaronjanse/gcat)

## Installation
```shell
$ git clone https://git.littleham.net/gmenu
$ cd gmenu
# make install
```

## Usage
```shell
$ gmenu gemini.circumlunar.space
```

## Contributing
Pull requests are welcome.\
Please do not open issues yet, as this is just a proof-of-concept as of now.

## License
[MIT](https://choosealicense.com/licenses/mit/)
