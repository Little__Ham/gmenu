PROGRAM = gmenu
PREFIX = /usr/local
BIN = $(PREFIX)/bin
MAN = $(PREFIX)/share/man

install: $(PROGRAM) $(PROGRAM).1
	mkdir -p $(BIN) $(MAN)/man1
	chmod +x $(PROGRAM)
	cp -f $(PROGRAM) $(BIN)
	cp -f $(PROGRAM).1 $(MAN)/man1

uninstall: 
	rm -f $(BIN)/$(PROGRAM) $(MAN)/man1/$(PROGRAM).1

lint: all
	shellcheck $(PROGRAM)
